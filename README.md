Very Secure FTP Daemon Docker in Docker
=======================================

## Informations ##

### File Structure ###

	.
	├─── build  # Data used to build containers (i.e., DockerFile)
	├─── data   # Raw data used by services
	└─── log    # Logs issued by services

## Setup ##

### Requirements ###

- [Docker](https://docs.docker.com/engine/installation/);
- [Docker Compose](https://docs.docker.com/compose/install/);
- Less time to prepare environment, more time to develop.

### Build ###

Build of containers based on docker-compose.yml:

```bash
docker-compose pull
docker-compose build
```

## Start ##

To get it up, please consider using:

```bash
docker-compose up -d
```

## Live display of logs ##

```bash
docker-compose logs --follow
```

## Run shell on container ##

Template:

```bash
docker-compose exec SERVICE COMMAND
```

Example:

```bash
docker-compose exec ftp sh
```

Then you will be able to manage your configuration files, debug daemons and much more...
