#!/bin/sh

###
# Configuration
#

# Update network config
sed -r -i 's/^[# \t]*(pasv_min_port)=.+$/\1='${PASV_MIN}'/;
           s/^[# \t]*(pasv_max_port)=.+$/\1='${PASV_MAX}'/;
           s/^[# \t]*(pasv_address)=.+$/\1='${PASV_ADDRESS}'/' /etc/vsftpd/vsftpd.conf

# Create user
addgroup -g 433 -S ${FTP_USER}
adduser -u 431 -D -G ${FTP_USER} -h /home/${FTP_USER} -s /bin/false ${FTP_USER}
echo "${FTP_USER}:${FTP_PASS}" | chpasswd
chown -R ${FTP_USER}:${FTP_USER} /home/${FTP_USER}/

###
# vsftpd
#
vsftpd /etc/vsftpd/vsftpd.conf

##
# logs
#
tail -F /var/log/vsftpd.log
